======
README
======

Summary
=======

- There are three tutorial datasets in this repository
- One each of Affymetrix, Agilent and Illumina
- The accompanying scripts serve as a walkthrough of the pipeline
- All the raw data has already been set up, so we can just normalise directly
- These datasets are currently located in our pipeline_ server
- ``samples/do_detailed_pipeline_affymetrix_genechip_6756.sh`` is a more detailed walkthrough

.. _pipeline: pipeline.stemformatics.org:/data/datasets/test/

Usage
=====

1. First we need to make a local copy of this dataset.
2. This is done within the script; set the path to your local directory of choice and uncomment the specified line.
3. Run the script.
4. Observe stdout; the script will print out various comments at key stages.
5. When complete, examine the files generated.

.. Note:: In this tutorial all raw data and metadata have already been set up and configured correctly. This tutorial is meant to showcase the steps in the pipeline only.


Utils
=====

Utils contains a series of convenient snippets to streamline data processing. All are works in progress.
